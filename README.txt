Diceware module
===============

Diceware offers a better way to create a strong, yet easy to remember
passphrase for use with encryption and security programs. Weak passwords and
passphrases are one of the most common flaws in computer security. [^1]

To help user to find a secure password by their selfs a list of samples
generated from a wordlist can be presented in a block.

This module uses following wordlists taken from the diceware-homepage:

English: diceware.wordlist.asc
:   provided by Arnold G. Reinhold under the terms of the Creative Commons
    CC-BY 3.0 license

German: diceware_german.txt
:   provided by Benjamin Tenne under the terms of the GNU General Public
    License


Installation
------------
Install as an usual module. There are no external dependencies.


Configuration
------------
In the menu "Administration >> Structure >> Blocks" you will find a block called
"Diceware sample passwords" or use the link
admin/structure/block/manage/diceware/diceware-block/configure

Next to the ussual settings of blocks you can customize following settings from
this module:

1. Number of password-samples to diplay.
2. Number of words a password consists of.
3. Descriptional text to show below the passphrase-sample.
4. The text-format of the text above.



[^1]: Quote from the homepage: <http://world.std.com/~reinhold/diceware.html>
